# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import fields
from trytond.pool import PoolMeta

__all__ = ['GnuHealthSequences']


class GnuHealthSequences(metaclass=PoolMeta):
    "GNU Health Sequences"
    __name__ = "gnuhealth.sequences"
    imaging_request_sequence = fields.Many2One('ir.sequence',
        'Imaging Request Sequence', required=True,
        domain=[('code', '=', 'gnuhealth.imaging.test.request')],
    )
    imaging_sequence = fields.Many2One('ir.sequence', 'Imaging Sequence',
        domain=[('code', '=', 'gnuhealth.imaging.test.result')],
        required=True
    )

    # @classmethod
    # def multivalue_model(cls, field):
    #     pool = Pool()
    #
    #     if field in sequences:
    #         return pool.get('gnuhealth.sequence.setup')
    #     return super(GnuHealthSequences, cls).multivalue_model(field)

    # @classmethod
    # def default_imaging_request_sequence(cls):
    #     return cls.multivalue_model(
    #         'imaging_request_sequence').default_imaging_request_sequence()

    # @classmethod
    # def default_imaging_sequence(cls):
    #     return cls.multivalue_model(
    #         'imaging_sequence').default_imaging_sequence()
